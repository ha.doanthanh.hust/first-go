package main

import (
	"net/http"

	"gitlab.com/ha.doanthanh.hust/first-go/src/controllers"
)

func main() {
	controllers.RegisterControllers()
	println(`Server listened on port: 3000`)
	http.ListenAndServe(":3000", nil)
}
